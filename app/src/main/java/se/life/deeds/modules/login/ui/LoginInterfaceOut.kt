package se.life.deeds.modules.login.ui

/**
 * Created by Christer Ulfsparre on 2018-1-23
 */

interface LoginInterfaceOut
{
    fun didShow()
}
