package se.life.deeds.modules.main.ui

import kotlinx.android.synthetic.main.fragment_main.*
import se.life.deeds.dataModel.JourneySegment
import se.life.deeds.extensions.views.setVerticalAdapater
import se.life.deeds.modules.base.DeedsFragment
import se.life.deeds.modules.main.ui.adapter.JourneyAdapter

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class MainFragment : DeedsFragment(), MainInterfaceIn
{
    lateinit var presenter: MainInterfaceOut
    val adapter = JourneyAdapter {  }

    override fun onLoad()
    {
        this.list.setVerticalAdapater(this.adapter)
    }

    override fun onShow()
    {
        this.presenter.didShow()
        this.adapter.show(listOf(
                JourneySegment(),
                JourneySegment(),
                JourneySegment(),
                JourneySegment(),
                JourneySegment()))
    }
}
