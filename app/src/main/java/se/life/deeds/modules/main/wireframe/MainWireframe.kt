package se.life.deeds.modules.main.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.main.presenter.MainPresenter
import se.life.deeds.modules.main.presenter.interactor.MainInteractor
import se.life.deeds.modules.main.ui.MainFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class MainWireframe : Wireframe()
{
    init
    {
        val fragment = MainFragment()
        val presenter = MainPresenter()
        val interactor = MainInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
