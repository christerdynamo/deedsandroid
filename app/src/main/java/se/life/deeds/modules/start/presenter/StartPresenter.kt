package se.life.deeds.modules.start.presenter

import se.life.deeds.modules.start.presenter.interactor.StartInteractor
import se.life.deeds.modules.start.ui.StartInterfaceIn
import se.life.deeds.modules.start.ui.StartInterfaceOut
import se.life.deeds.modules.start.wireframe.StartWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class StartPresenter : StartInterfaceOut
{
    lateinit var wireframe: StartWireframe
    lateinit var ui: StartInterfaceIn
    lateinit var interactor: StartInteractor

    override fun didShow()
    {
    }
}
