package se.life.deeds.modules.main.presenter

import se.life.deeds.modules.main.presenter.interactor.MainInteractor
import se.life.deeds.modules.main.ui.MainInterfaceIn
import se.life.deeds.modules.main.ui.MainInterfaceOut
import se.life.deeds.modules.main.wireframe.MainWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class MainPresenter : MainInterfaceOut
{
    lateinit var wireframe: MainWireframe
    lateinit var ui: MainInterfaceIn
    lateinit var interactor: MainInteractor

    override fun didShow()
    {
    }
}
