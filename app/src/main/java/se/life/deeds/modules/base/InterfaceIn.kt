package se.life.deeds.modules.base

import se.life.deeds.helpers.dialog.Dialog

/**
 * Created by christer on 2018-01-23.
 */
enum class Dropdown
{
    Error,
    Success
}

interface InterfaceIn
{
    fun showTitle(resId: Int?)
    fun showTitle(title: String?)
    fun showDropdown(resId: Int?, type: Dropdown)
    fun showDropdown(message: String?, type: Dropdown)
    fun showDialog(constructor: (Dialog.DialogBuilderLight) -> (Unit))
}
