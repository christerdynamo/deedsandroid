package se.life.deeds.modules.resetPassword.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.resetPassword.presenter.ResetPasswordPresenter
import se.life.deeds.modules.resetPassword.presenter.interactor.ResetPasswordInteractor
import se.life.deeds.modules.resetPassword.ui.ResetPasswordFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ResetPasswordWireframe : Wireframe()
{
    init
    {
        val fragment = ResetPasswordFragment()
        val presenter = ResetPasswordPresenter()
        val interactor = ResetPasswordInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
