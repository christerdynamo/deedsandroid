package se.life.deeds.modules.login.ui

import se.life.deeds.modules.base.InterfaceIn

/**
 * Created by Christer Ulfsparre on 2018-1-23
 */

interface LoginInterfaceIn: InterfaceIn
{
    fun showLoadingIndicator(show: Boolean)
}