package se.life.deeds.modules.onboardingForm.ui

import se.life.deeds.modules.base.InterfaceIn

/**
 * Created by Christer Ulfsparre on 2018-1-30
 */

interface OnboardingFormInterfaceIn: InterfaceIn
{
}