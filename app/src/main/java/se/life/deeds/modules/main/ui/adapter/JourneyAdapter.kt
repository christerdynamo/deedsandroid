package se.life.deeds.modules.main.ui.adapter

import se.life.deeds.R
import se.life.deeds.dataModel.JourneySegment
import se.life.deeds.modules.base.adapters.GenericRecyclerViewAdapter

/**
 * Created by christer on 2018-01-29.
 */
class JourneyAdapter(onSelect: (JourneySegment) -> Unit) : GenericRecyclerViewAdapter<JourneySegment>({ _, index ->

    if(index % 2 == 0)
    {
        R.layout.row_journey_left
    }
    else
    {
        R.layout.row_journey_right
    }

}, { row, item ->

    

}, onSelect)