package se.life.deeds.modules.login.presenter

import se.life.deeds.modules.login.presenter.interactor.LoginInteractor
import se.life.deeds.modules.login.ui.LoginInterfaceIn
import se.life.deeds.modules.login.ui.LoginInterfaceOut
import se.life.deeds.modules.login.wireframe.LoginWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-23
 */

class LoginPresenter : LoginInterfaceOut
{
    lateinit var wireframe: LoginWireframe
    lateinit var ui: LoginInterfaceIn
    lateinit var interactor: LoginInteractor

    override fun didShow()
    {
    }
}
