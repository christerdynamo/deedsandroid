package se.life.deeds.modules.start.ui

import se.life.deeds.modules.base.DeedsFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class StartFragment : DeedsFragment(), StartInterfaceIn
{
    lateinit var presenter: StartInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()
    }
}
