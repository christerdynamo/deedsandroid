package se.life.deeds.modules.base

import android.support.v4.app.Fragment
import se.life.deeds.helpers.router.Router

/**
 * Created by christer on 2018-01-23
 */
open class Wireframe
{
    lateinit var fragment: Fragment

    protected fun push(fragment: Fragment)
    {
        Router.push(fragment)
    }

    protected fun pop()
    {
        Router.pop()
    }

    protected fun popToRoot()
    {
        Router.popToRoot()
    }

    protected fun switchTab(i: Int)
    {
        Router.switchTab(i)
    }

    protected fun show(fragment: Fragment)
    {
        Router.show(fragment)
    }

    protected fun dismiss()
    {
        Router.dismiss()
    }
}
