package se.life.deeds.modules.forgotPassword.ui

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface ForgotPasswordInterfaceOut
{
    fun didShow()
}
