package se.life.deeds.modules.login.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.login.presenter.LoginPresenter
import se.life.deeds.modules.login.presenter.interactor.LoginInteractor
import se.life.deeds.modules.login.ui.LoginFragment

/**
 * Created by Christer Ulfsparre on 2018-1-23
 */

class LoginWireframe : Wireframe()
{
    init
    {
        val fragment = LoginFragment()
        val presenter = LoginPresenter()
        val interactor = LoginInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
