package se.life.deeds.modules.onboardingForm.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.onboardingForm.presenter.OnboardingFormPresenter
import se.life.deeds.modules.onboardingForm.presenter.interactor.OnboardingFormInteractor
import se.life.deeds.modules.onboardingForm.ui.OnboardingFormFragment

/**
 * Created by Christer Ulfsparre on 2018-1-30
 */

class OnboardingFormWireframe : Wireframe()
{
    init
    {
        val fragment = OnboardingFormFragment()
        val presenter = OnboardingFormPresenter()
        val interactor = OnboardingFormInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
