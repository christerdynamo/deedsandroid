package se.life.deeds.modules.forgotPassword.presenter

import se.life.deeds.modules.forgotPassword.presenter.interactor.ForgotPasswordInteractor
import se.life.deeds.modules.forgotPassword.ui.ForgotPasswordInterfaceIn
import se.life.deeds.modules.forgotPassword.ui.ForgotPasswordInterfaceOut
import se.life.deeds.modules.forgotPassword.wireframe.ForgotPasswordWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ForgotPasswordPresenter : ForgotPasswordInterfaceOut
{
    lateinit var wireframe: ForgotPasswordWireframe
    lateinit var ui: ForgotPasswordInterfaceIn
    lateinit var interactor: ForgotPasswordInteractor

    override fun didShow()
    {
    }
}
