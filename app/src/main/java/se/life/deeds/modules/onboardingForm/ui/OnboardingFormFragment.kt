package se.life.deeds.modules.onboardingForm.ui

import se.life.deeds.modules.base.DeedsFragment

/**
 * Created by Christer Ulfsparre on 2018-1-30
 */

class OnboardingFormFragment : DeedsFragment(), OnboardingFormInterfaceIn
{
    lateinit var presenter: OnboardingFormInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()
    }
}
