package se.life.deeds.modules.base.adapters

import android.view.View

/**
 * Created by christer on 2018-01-29.
 */
class SimpleGenericRecyclerViewAdapter<T>(layout: Int,
                                          style: (View, T) -> (Unit),
                                          select: ((T) -> (Unit))? = null):
        GenericRecyclerViewAdapter<T>({ _, _ -> layout }, style, select)
