package se.life.deeds.modules.forgotPassword.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.forgotPassword.presenter.ForgotPasswordPresenter
import se.life.deeds.modules.forgotPassword.presenter.interactor.ForgotPasswordInteractor
import se.life.deeds.modules.forgotPassword.ui.ForgotPasswordFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ForgotPasswordWireframe : Wireframe()
{
    init
    {
        val fragment = ForgotPasswordFragment()
        val presenter = ForgotPasswordPresenter()
        val interactor = ForgotPasswordInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
