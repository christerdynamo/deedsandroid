package se.life.deeds.modules.resetPassword.ui

import se.life.deeds.modules.base.DeedsFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ResetPasswordFragment : DeedsFragment(), ResetPasswordInterfaceIn
{
    lateinit var presenter: ResetPasswordInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()
    }
}
