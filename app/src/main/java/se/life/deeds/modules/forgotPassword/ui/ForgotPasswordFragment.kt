package se.life.deeds.modules.forgotPassword.ui

import se.life.deeds.modules.base.DeedsFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ForgotPasswordFragment : DeedsFragment(), ForgotPasswordInterfaceIn
{
    lateinit var presenter: ForgotPasswordInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()
    }
}
