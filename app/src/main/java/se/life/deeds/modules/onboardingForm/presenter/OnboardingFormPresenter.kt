package se.life.deeds.modules.onboardingForm.presenter

import se.life.deeds.modules.onboardingForm.presenter.interactor.OnboardingFormInteractor
import se.life.deeds.modules.onboardingForm.ui.OnboardingFormInterfaceIn
import se.life.deeds.modules.onboardingForm.ui.OnboardingFormInterfaceOut
import se.life.deeds.modules.onboardingForm.wireframe.OnboardingFormWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-30
 */

class OnboardingFormPresenter : OnboardingFormInterfaceOut
{
    lateinit var wireframe: OnboardingFormWireframe
    lateinit var ui: OnboardingFormInterfaceIn
    lateinit var interactor: OnboardingFormInteractor

    override fun didShow()
    {
    }
}
