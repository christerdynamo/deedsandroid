package se.life.deeds.modules.start.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.start.presenter.StartPresenter
import se.life.deeds.modules.start.presenter.interactor.StartInteractor
import se.life.deeds.modules.start.ui.StartFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class StartWireframe : Wireframe()
{
    init
    {
        val fragment = StartFragment()
        val presenter = StartPresenter()
        val interactor = StartInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
