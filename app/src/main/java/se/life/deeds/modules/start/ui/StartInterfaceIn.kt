package se.life.deeds.modules.start.ui

import se.life.deeds.modules.base.InterfaceIn

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface StartInterfaceIn: InterfaceIn
{
}