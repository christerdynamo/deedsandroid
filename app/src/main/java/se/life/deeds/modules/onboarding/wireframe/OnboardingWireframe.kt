package se.life.deeds.modules.onboarding.wireframe

import android.support.v4.app.Fragment

import se.life.deeds.modules.base.Wireframe
import se.life.deeds.modules.onboarding.presenter.OnboardingPresenter
import se.life.deeds.modules.onboarding.presenter.interactor.OnboardingInteractor
import se.life.deeds.modules.onboarding.ui.OnboardingFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class OnboardingWireframe : Wireframe()
{
    init
    {
        val fragment = OnboardingFragment()
        val presenter = OnboardingPresenter()
        val interactor = OnboardingInteractor()
        presenter.wireframe = this
        presenter.ui = fragment
        presenter.interactor = interactor
        fragment.presenter = presenter
        this.fragment = fragment
    }
}
