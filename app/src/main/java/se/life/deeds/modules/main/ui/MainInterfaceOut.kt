package se.life.deeds.modules.main.ui

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface MainInterfaceOut
{
    fun didShow()
}
