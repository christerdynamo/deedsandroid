package se.life.deeds.modules.resetPassword.presenter

import se.life.deeds.modules.resetPassword.presenter.interactor.ResetPasswordInteractor
import se.life.deeds.modules.resetPassword.ui.ResetPasswordInterfaceIn
import se.life.deeds.modules.resetPassword.ui.ResetPasswordInterfaceOut
import se.life.deeds.modules.resetPassword.wireframe.ResetPasswordWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class ResetPasswordPresenter : ResetPasswordInterfaceOut
{
    lateinit var wireframe: ResetPasswordWireframe
    lateinit var ui: ResetPasswordInterfaceIn
    lateinit var interactor: ResetPasswordInteractor

    override fun didShow()
    {
    }
}
