package se.life.deeds.modules.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import java.util.regex.Pattern

/**
 * Created by christer on 2018-01-23
 *
 * - Automatically creates layout based on class name.
 */
abstract class BaseFragment : Fragment()
{
    protected var menu: Menu? = null

    abstract fun onShow()

    open fun onHide()
    {
    }

    open fun onLoad()
    {
    }

    open fun onUnload()
    {
    }

    open fun onPressMenuItem(id: Int)
    {
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        val resID = context?.resources?.getIdentifier(this.menuName(), "menu", context?.packageName)
        this.setHasOptionsMenu(resID ?: 0 > 0)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        val resID = context?.resources?.getIdentifier(this.layoutName(), "layout", context?.packageName)
        if(resID != null)
        {
            return inflater.inflate(resID, container, false)
        }
        return null
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?)
    {
        try
        {
            val resID = context?.resources?.getIdentifier(this.menuName(), "menu", context?.packageName)
            if(resID != null)
            {
                inflater?.inflate(resID, menu)
                this.menu = menu
            }
        }
        catch (e: InflateException)
        {
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean
    {
        if(item?.itemId != null)
        {
            this.onPressMenuItem(item.itemId)
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        this.onLoad()
    }

    override fun onDestroyView()
    {
        super.onDestroyView()
        this.onUnload()
    }

    override fun onResume()
    {
        super.onResume()
        this.onShow()
    }

    override fun onPause()
    {
        super.onPause()
        this.onHide()
    }

    private fun baseName(): String
    {
        val pattern = Pattern.compile("(?<=.)(?=\\p{Lu})")
        return pattern.split(this.removeSuffix(this.javaClass.simpleName, "Fragment")).joinToString("_").toLowerCase()
    }

    private fun layoutName(): String = "fragment_${this.baseName()}"
    private fun menuName(): String = "menu_${this.baseName()}"

    private fun removeSuffix(text: String,
                             suffix: String): String
    {
        if (text.endsWith(suffix))
        {
            return text.substring(0, text.length - suffix.length)
        }
        return text
    }
}
