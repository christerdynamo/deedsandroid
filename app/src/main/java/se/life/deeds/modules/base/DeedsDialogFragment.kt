package se.life.deeds.modules.base

import se.life.deeds.extensions.views.showErrorDropdown
import se.life.deeds.extensions.views.showSuccessDropdown
import se.life.deeds.helpers.dialog.Dialog

/**
 * Created by christer on 2018-01-23.
 */
abstract class DeedsDialogFragment : BaseDialogFragment()
{
    override fun showTitle(resId: Int?)
    {
        if (resId != null)
        {
            this.activity?.title = getString(resId)
        }
        else
        {
            this.activity?.title = null
        }
    }

    override fun showTitle(title: String?)
    {
        this.activity?.title = title
    }

    override fun showDropdown(resId: Int?, type: Dropdown)
    {
        if (resId != null)
        {
            this.showDropdown(getString(resId), type)
        }
    }

    override fun showDropdown(message: String?, type: Dropdown)
    {
        if (type == Dropdown.Error)
        {
            this.showErrorDropdown(message)
        }
        else if (type == Dropdown.Success)
        {
            this.showSuccessDropdown(message)
        }
    }

    override fun showDialog(constructor: (Dialog.DialogBuilderLight) -> Unit)
    {
        val activity = this.activity
        if (activity != null)
        {
            Dialog(activity, constructor)
        }
    }

    fun showActionBar(show: Boolean)
    {
        if (show)
        {
            this.activity?.actionBar?.show()
        }
        else
        {
            this.activity?.actionBar?.hide()
        }
    }
}
