package se.life.deeds.modules.resetPassword.ui

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface ResetPasswordInterfaceOut
{
    fun didShow()
}
