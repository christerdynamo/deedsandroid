package se.life.deeds.modules.onboardingForm.ui

/**
 * Created by Christer Ulfsparre on 2018-1-30
 */

interface OnboardingFormInterfaceOut
{
    fun didShow()
}
