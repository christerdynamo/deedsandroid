package se.life.deeds.modules.onboarding.ui

import se.life.deeds.modules.base.DeedsFragment

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class OnboardingFragment : DeedsFragment(), OnboardingInterfaceIn
{
    lateinit var presenter: OnboardingInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()
    }
}
