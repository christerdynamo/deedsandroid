package se.life.deeds.modules.onboarding.presenter

import se.life.deeds.modules.onboarding.presenter.interactor.OnboardingInteractor
import se.life.deeds.modules.onboarding.ui.OnboardingInterfaceIn
import se.life.deeds.modules.onboarding.ui.OnboardingInterfaceOut
import se.life.deeds.modules.onboarding.wireframe.OnboardingWireframe

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

class OnboardingPresenter : OnboardingInterfaceOut
{
    lateinit var wireframe: OnboardingWireframe
    lateinit var ui: OnboardingInterfaceIn
    lateinit var interactor: OnboardingInteractor

    override fun didShow()
    {
    }
}
