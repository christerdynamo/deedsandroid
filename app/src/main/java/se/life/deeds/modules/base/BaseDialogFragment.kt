package se.life.deeds.modules.base

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.regex.Pattern

/**
 * Created by christer on 2018-01-23.
 *
 * - Automatically creates layout based on class name.
 */
abstract class BaseDialogFragment: DialogFragment(), InterfaceIn
{
    abstract fun onShow()

    open fun onHide()
    {
    }

    open fun onLoad()
    {
    }

    open fun onUnload()
    {
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        val resID = context?.resources?.getIdentifier(this.menuName(), "menu", context?.packageName)
        this.setHasOptionsMenu(resID ?: 0 > 0)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        val resID = context?.resources?.getIdentifier(this.layoutName(), "layout", context?.packageName)
        if(resID != null)
        {
            return inflater.inflate(resID, container, false)
        }
        return null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        this.onLoad()
    }

    override fun onDestroyView()
    {
        super.onDestroyView()
        this.onUnload()
    }

    override fun onResume()
    {
        super.onResume()
        this.onShow()
    }

    override fun onPause()
    {
        super.onPause()
        this.onHide()
    }

    private fun baseName(): String
    {
        val pattern = Pattern.compile("(?<=.)(?=\\p{Lu})")
        return pattern.split(this.removeSuffix(this.javaClass.simpleName, "Fragment")).joinToString("_").toLowerCase()
    }

    private fun layoutName(): String = "fragment_${this.baseName()}"
    private fun menuName(): String = "menu_${this.baseName()}"

    private fun removeSuffix(text: String,
                             suffix: String): String
    {
        if (text.endsWith(suffix))
        {
            return text.substring(0, text.length - suffix.length)
        }
        return text
    }
}
