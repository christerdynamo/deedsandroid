package se.life.deeds.modules.start.ui

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface StartInterfaceOut
{
    fun didShow()
}
