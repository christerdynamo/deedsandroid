package se.life.deeds.modules.base.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.life.deeds.extensions.views.onClick

/**
 * Created by christer on 2018-01-23.
 */
open class GenericRecyclerViewAdapter<T>(layout: (T, Int) -> (Int),
                                         style: (View, T) -> (Unit),
                                         select: ((T) -> (Unit))? = null) : RecyclerView.Adapter<GenericRecyclerViewAdapter.ViewHolder>()
{
    private val layout: (T, Int) -> (Int)
    private var style: (View, T) -> (Unit)
    private var select: ((T) -> (Unit))? = null
    private var items: List<T>? = null

    init
    {
        this.layout = layout
        this.style = style
        this.select = select
    }

    fun show(items: List<T>?)
    {
        this.items = items
        this.notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int
    {
        this.items?.getOrNull(position)?.let {
            return this.layout(it, position)
        }
        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val v = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int
    {
        val items = this.items
        if (items != null)
        {
            return items.count()
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        val item = this.items?.getOrNull(position)
        if (item != null)
        {
            this.style(holder.itemView, item)
            if (this.select != null)
            {
                holder.itemView.onClick { this.select?.invoke(item) }
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
    }
}
