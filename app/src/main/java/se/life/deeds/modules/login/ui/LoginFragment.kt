package se.life.deeds.modules.login.ui

import android.graphics.BitmapFactory
import kotlinx.android.synthetic.main.fragment_login.*
import se.life.deeds.R
import se.life.deeds.modules.base.DeedsFragment
import se.life.deeds.views.FootprintView.Type

/**
 * Created by Christer Ulfsparre on 2018-1-23
 */

class LoginFragment : DeedsFragment(), LoginInterfaceIn
{
    lateinit var presenter: LoginInterfaceOut

    override fun onShow()
    {
        this.presenter.didShow()

        val info = listOf(
                Type(10f, BitmapFactory.decodeResource(resources, R.mipmap.ic_airplanemode_active_black_24dp)),
                Type(11f, BitmapFactory.decodeResource(resources, R.mipmap.ic_airport_shuttle_black_24dp)),
                Type(18f, BitmapFactory.decodeResource(resources, R.mipmap.ic_business_black_24dp)),
                Type(20f, BitmapFactory.decodeResource(resources, R.mipmap.ic_directions_bus_black_24dp)),
                Type(10f, BitmapFactory.decodeResource(resources, R.mipmap.ic_local_play_black_24dp))
        )

        this.footprint.showInfo(info)
    }

    override fun showLoadingIndicator(show: Boolean)
    {
    }
}
