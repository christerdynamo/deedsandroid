package se.life.deeds.modules.onboarding.ui

/**
 * Created by Christer Ulfsparre on 2018-1-29
 */

interface OnboardingInterfaceOut
{
    fun didShow()
}
