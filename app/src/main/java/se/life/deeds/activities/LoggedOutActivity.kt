package se.life.deeds.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ncapdevi.fragnav.FragNavController
import com.ncapdevi.fragnav.FragNavTransactionOptions
import se.life.deeds.R
import se.life.deeds.helpers.router.Router
import se.life.deeds.modules.main.wireframe.MainWireframe

/**
 * Created by christer on 2018-01-23.
 */
class LoggedOutActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logged_out)
        val builder = FragNavController.newBuilder(savedInstanceState, supportFragmentManager, R.id.container)

        val wireframe = MainWireframe()
        builder.rootFragment(wireframe.fragment)

        builder.defaultTransactionOptions(FragNavTransactionOptions.newBuilder()
                .customAnimations(
                        R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left,
                        R.anim.slide_in_from_left,
                        R.anim.slide_out_to_right).build()
        )

        Router.set(builder.build())
    }

    override fun onBackPressed()
    {
        if (!Router.pop())
        {
            super.onBackPressed()
        }
    }
}