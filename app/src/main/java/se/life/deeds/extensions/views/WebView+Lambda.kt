package se.life.deeds.extensions.views

import android.graphics.Bitmap
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

/**
 * Created by christer on 2018-01-23.
 */
enum class WebViewState
{
    ON_STARTED,
    ON_FINISHED,
    ON_ERROR
}

fun WebView.onStateChanged(onState: (WebViewState) -> (Unit))
{
    this.webViewClient = object : WebViewClient()
    {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?)
        {
            super.onPageStarted(view, url, favicon)
            onState(WebViewState.ON_STARTED)
        }

        override fun onPageFinished(view: WebView?, url: String?)
        {
            super.onPageFinished(view, url)
            onState(WebViewState.ON_FINISHED)
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?)
        {
            super.onReceivedError(view, request, error)
            onState(WebViewState.ON_ERROR)
        }
    }
}
