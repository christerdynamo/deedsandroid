package se.life.deeds.extensions.networking

import se.life.deeds.application.DeedsApplication
import se.life.deeds.application.DeedsApplication.Companion.getString
import se.life.deeds.utilities.promises.Deferred
import se.life.deeds.utilities.promises.Promise

/**
 * Created by christer on 2018-01-23.
 */

infix fun <V, R> Promise<V, Status>.then(bind: (V) -> Promise<R, Status>): Promise<R, Status>
{
    val deferred = Deferred<R, Status>()
    success {
        bind(it) success {
            deferred.resolve(it)
        } fail {
            deferred.reject(it)
        }
    } fail {
        deferred.reject(it)
    }

    return deferred.promise()
}

infix fun <V> Promise<V, Status>.fallback(bind: () -> Promise<V, Status>): Promise<V, Status>
{
    val deferred = Deferred<V, Status>()
    success {
        deferred.resolve(it)
    } fail {
        bind() success {
            deferred.resolve(it)
        } fail {
            deferred.reject(it)
        }
    }

    return deferred.promise()
}

fun <V>Promise<V?, Status>.noNull(resId: Int): Promise<V, Status>
{
    return noNull(getString(resId))
}

fun <V>Promise<V?, Status>.noNull(message: String? = null): Promise<V, Status>
{
    val deferred = Deferred<V, Status>()
    success {
        if(it != null)
        {
            deferred.resolve(it)
        }
        else
        {
            deferred.reject(Status.Error(message))
        }
    } fail {
        deferred.reject(it)
    }

    return deferred.promise()
}

fun <V: Collection<Any>>Promise<V, Status>.noEmpty(resId: Int? = null): Promise<V, Status>
{
    return noEmpty(DeedsApplication.getString(resId))
}

fun <V: Collection<Any>>Promise<V, Status>.noEmpty(message: String?): Promise<V, Status>
{
    val deferred = Deferred<V, Status>()
    success {
        if(it.isNotEmpty())
        {
            deferred.resolve(it)
        }
        else
        {
            deferred.reject(Status.Error(message))
        }
    } fail {
        deferred.reject(it)
    }

    return deferred.promise()
}

fun <V: Collection<Any>>Promise<V?, Status>.noNullOrEmpty(resId: Int? = null): Promise<V, Status>
{
    return noNullOrEmpty(DeedsApplication.getString(resId))
}

fun <V: Collection<Any>>Promise<V?, Status>.noNullOrEmpty(message: String?): Promise<V, Status>
{
    val deferred = Deferred<V, Status>()
    success {
        if(it != null && it.isNotEmpty())
        {
            deferred.resolve(it)
        }
        else
        {
            deferred.reject(Status.Error(message))
        }
    } fail {
        deferred.reject(it)
    }

    return deferred.promise()
}

fun <V, R> Promise<V, Status>.collect(collect: (V) -> R): Promise<R, Status>
{
    val deferred = Deferred<R, Status>()
    success {
        try
        {
            deferred.resolve(collect(it))
        }
        catch (e: Exception)
        {
            deferred.reject(Status.Error())
        }
    } fail {
        deferred.reject(it)
    }
    return deferred.promise()
}
