package se.life.deeds.extensions.networking

import okhttp3.Response
import okio.Buffer

/**
 * Created by christer on 2018-01-23.
 */

fun Response.curl(): String
{
    val builder = StringBuilder("")
    builder.append("cURL ")
    builder.append("-X ")

    builder.append(this.request().method().toUpperCase() + " ")

    for(headerName in this.request().headers().names())
    {
        builder.append("-H " + "\"" + headerName + ": " + this.request().headers().get(headerName) + "\" ")
    }

    val requestBody = this.request().body()
    if(requestBody != null)
    {
        val buffer = Buffer()
        requestBody.writeTo(buffer)
        val contentType = requestBody.contentType()
        if(contentType != null)
        {
            builder.append("-H " + "\"" + "Content-Type" + ": " + contentType.toString() + "\" ")
            val charset = contentType.charset()
            if(charset != null)
            {
                builder.append(" -d '" + buffer.readString(charset) + "'")
            }
        }
    }

    builder.append(" \"" + this.request().url().toString() + "\"")
    builder.append(" -L")

    return builder.toString()
}
