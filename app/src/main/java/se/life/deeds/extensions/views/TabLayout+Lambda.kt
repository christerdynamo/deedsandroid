package se.life.deeds.extensions.views

import android.support.design.widget.TabLayout

/**
 * Created by christer on 2018-01-23.
 */
fun TabLayout.onSelect(onSelect: (Int) -> (Unit))
{
    this.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener
    {
        override fun onTabSelected(tab: TabLayout.Tab?)
        {
            if(tab != null)
            {
                onSelect(tab.position)
            }
        }

        override fun onTabReselected(tab: TabLayout.Tab?)
        {
        }

        override fun onTabUnselected(tab: TabLayout.Tab?)
        {
        }
    })
}
