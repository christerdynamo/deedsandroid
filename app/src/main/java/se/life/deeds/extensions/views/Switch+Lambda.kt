package se.life.deeds.extensions.views

import android.widget.Switch

/**
 * Created by christer on 2018-01-23.
 */
fun Switch.onChange(onChange: (Boolean) -> Unit)
{
    this.setOnCheckedChangeListener { _, on ->
        onChange(on)
    }
}
