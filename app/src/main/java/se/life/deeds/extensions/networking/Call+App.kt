package se.life.deeds.extensions.networking

import se.life.deeds.R
import se.life.deeds.utilities.promises.Deferred
import se.life.deeds.utilities.promises.Promise
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import se.life.deeds.application.DeedsApplication.Companion.getString

/**
 * Created by christer on 2018-01-23
 */
fun <T> Call<T>.on(success: (T, Status) -> (Unit),
                   failure: (Status) -> (Unit))
{
    this.enqueue(object : Callback<T>
    {
        override fun onResponse(call: Call<T>?, response: Response<T>?)
        {
            val result = response?.body()
            if(response?.isSuccessful() ?: false && result != null)
            {
                success(result, Status(response = null, code = response.code()))
            }
            else
            {
                failure(Status(response?.errorBody(), response?.code() ?: 400))
            }
        }

        override fun onFailure(call: Call<T>?, t: Throwable?)
        {
            failure(Status(response = null, code = 408))
        }
    })
}

fun <T> Call<T>.onNullable(success: (T?, Status) -> (Unit),
                           failure: (Status) -> (Unit))
{
    this.enqueue(object : Callback<T>
    {
        override fun onResponse(call: Call<T>?, response: Response<T>?)
        {
            if (response?.isSuccessful() ?: false)
            {
                success(response?.body(), Status(response = null, code = response?.code() ?: 200))
            }
            else
            {
                failure(Status(response?.errorBody(), response?.code() ?: 400))
            }
        }

        override fun onFailure(call: Call<T>?, t: Throwable?)
        {
            failure(Status(response = null, code = 408))
        }
    })
}

fun <T> Call<T>.promise(): Promise<T, Status>
{
    val deferred = Deferred<T, Status>()
    this.on({ result, status ->
        try
        {
            deferred.resolve(result)
        }
        catch (e: Exception)
        {
            deferred.reject(status)
        }
    }, {
        deferred.reject(it)
    })
    return deferred.promise()
}

fun <T> Call<T>.promiseVoid(): Promise<Unit, Status>
{
    val deferred = Deferred<Unit, Status>()
    this.onNullable({ _, status ->
        try
        {
            deferred.resolve(Unit)
        }
        catch (e: Exception)
        {
            deferred.reject(status)
        }
    }, {
        deferred.reject(it)
    })
    return deferred.promise()
}

class Status()
{
    private var message: String? = null
    private var hash = HashMap<String, List<String>>()

    var code: Int = 0
    var body: String? = null

    operator fun get(index: String?): String?
    {
        val errors = this.hash[index]
        if(errors != null && errors.size == 1)
        {
            return errors.first()
        }
        else if(errors != null && errors.size > 1)
        {
            return "• " + errors.joinToString("\n• ")
        }
        return null
    }

    fun message(): String?
    {
        return this.message
    }

    constructor(message: String?, code: Int): this()
    {
        this.message = message
        this.code = code
    }

    constructor(response: ResponseBody?, code: Int): this()
    {
        val hash: HashMap<String, List<String>> = HashMap()
        try
        {
            val json = JSONObject(body)
            json.keys().forEach { key ->
                val arr = json.getJSONArray(key)
                val list: ArrayList<String> = ArrayList()
                for (i in 0 until arr.length())
                {
                    val error = arr.get(i)
                    if(error is String)
                    {
                        list.add(error)
                    }
                }
                if(list.size > 0)
                {
                    hash[key] = list
                }
            }
        }
        catch (e: Exception)
        {
            hash.clear()
        }

        val list: ArrayList<String> = ArrayList()
        try
        {
            val json = JSONArray(body)
            if(json.length() > 0)
            {
                for (i in 0..(json.length() - 1))
                {
                    list.add(json.getString(i))
                }
            }
        }
        catch (e: Exception)
        {
            list.clear()
        }

        val errors = hash.values.flatMap { it }
        if(errors.size == 1)
        {
            this.message = errors.first()
        }
        else if(errors.size > 1)
        {
            this.message = "• " + errors.joinToString("\n• ")
        }
        else if(list.size == 1)
        {
            this.message = list.first()
        }
        else if(list.size > 1)
        {
            this.message = "• " + list.joinToString("\n• ")
        }
        else if(code >= 500)
        {
            this.message = getString(R.string.status_internalServerError)
        }
        else if(code == 408)
        {
            this.message = getString(R.string.status_requestTimeout)
        }
        else if(code >= 400)
        {
            this.message = getString(R.string.status_badRequest)
        }

        this.hash.putAll(hash)
        this.code = code
        this.body = response?.string()
    }

    companion object
    {
        fun Error(message: String? = null): Status
        {
            return Status(message = message, code = 400)
        }
    }
}
