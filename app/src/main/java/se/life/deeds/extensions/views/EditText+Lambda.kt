package se.life.deeds.extensions.views

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

/**
 * Created by christer on 2018-01-23.
 */
fun EditText.onChange(onChanged: (String) -> Unit)
{
    this.addTextChangedListener(object : TextWatcher
    {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
        {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int)
        {
        }

        override fun afterTextChanged(editable: Editable?)
        {
            onChanged(editable.toString())
        }
    })
}
