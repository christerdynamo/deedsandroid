package se.life.deeds.extensions.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by christer on 2018-01-23.
 */
fun String.date(format: String): Date?
{
    try
    {
        return SimpleDateFormat(format, Locale.getDefault()).parse(this)
    }
    catch (e: Exception)
    {
        return null
    }
}
