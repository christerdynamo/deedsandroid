package se.life.deeds.extensions.views

import android.view.View

/**
 * Created by christer on 2018-01-23.
 */
var View.isGone: Boolean
    get()
    {
        return this.visibility == View.GONE
    }
    set(value)
    {
        this.visibility = if(value) View.GONE else View.VISIBLE
    }

var View.isInvisible: Boolean
    get()
    {
        return this.visibility == View.INVISIBLE
    }
    set(value)
    {
        this.visibility = if(value) View.INVISIBLE else View.VISIBLE
    }
