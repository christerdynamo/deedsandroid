package se.life.deeds.extensions.views

import android.support.v4.app.Fragment
import com.tapadoo.alerter.Alerter
import se.life.deeds.R

/**
 * Created by christer on 2018-01-23
 */
fun Fragment.showErrorDropdown(message: String?)
{
    val activity = activity
    if((message ?: "").isNotEmpty() && activity != null)
    {
        Alerter.create(activity)
                .setText(message)
                .setBackgroundResource(R.color.error)
                .show()
    }
}

fun Fragment.showSuccessDropdown(message: String?)
{
    val activity = activity
    if((message ?: "").isNotEmpty() && activity != null)
    {
        Alerter.create(activity)
                .setText(message)
                .setBackgroundResource(R.color.success)
                .show()
    }
}
