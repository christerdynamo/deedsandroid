package se.life.deeds.extensions.views

import android.view.View

/**
 * Created by christer on 2017-11-03
 */
fun View.onClick(onClicked: () -> Unit)
{
    this.setOnClickListener { onClicked() }
}
