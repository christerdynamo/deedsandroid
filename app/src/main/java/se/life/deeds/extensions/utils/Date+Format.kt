package se.life.deeds.extensions.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by christer on 2018-01-23.
 */
fun Date.format(format: String): String?
        = SimpleDateFormat(format, Locale.getDefault()).format(this)
