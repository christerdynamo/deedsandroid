package se.life.deeds.extensions.utils

import android.graphics.PointF

/**
 * Created by christer on 2018-01-26.
 */
fun PointF.distance(pos: PointF): Float
{
    return Math.sqrt(Math.pow((this.x.toDouble() - pos.x.toDouble()), 2.0) + Math.pow(this.y.toDouble() - pos.y.toDouble(), 2.0)).toFloat()
}

fun PointF.move(angle: Float, distance: Float): PointF
{
    val radians = angle * Math.PI / 180.0f
    return PointF(this.x + Math.cos(radians).toFloat() * distance, this.y + Math.sin(radians).toFloat() * distance)
}

fun PointF.angle(pos: PointF): Float
{
    return Math.atan2(this.y.toDouble() - pos.y.toDouble(), this.x.toDouble() - pos.x.toDouble()).toFloat()
}

fun Float.degrees(): Float
{
    return this * (180.0f / Math.PI.toFloat())
}

fun Float.radians(): Float
{
    return this * (Math.PI.toFloat() / 180.0f)
}