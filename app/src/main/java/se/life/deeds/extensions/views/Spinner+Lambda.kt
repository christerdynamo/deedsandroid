package se.life.deeds.extensions.views

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner

/**
 * Created by christer on 2018-01-23.
 */
fun <T: Any>Spinner.onSelect(onSelect: (T) -> (Unit))
{
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
    {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long)
        {
            if(selectedItem != null)
            {
                @Suppress("UNCHECKED_CAST")
                onSelect(selectedItem as T)
            }
        }

        override fun onNothingSelected(p0: AdapterView<*>?)
        {
        }
    }
}
