package se.life.deeds.extensions.utils

import java.util.*

/**
 * Created by christer on 2018-01-23.
 */
fun Date.calendar(): Calendar
{
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.time
    return calendar
}
