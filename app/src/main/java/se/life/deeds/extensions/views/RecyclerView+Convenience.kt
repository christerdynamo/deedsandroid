package se.life.deeds.extensions.views

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

/**
 * Created by christer on 2018-01-23.
 */
fun <T: RecyclerView.ViewHolder>RecyclerView.setVerticalAdapater(adapter: RecyclerView.Adapter<T>)
{
    this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    this.adapter = adapter
}

fun <T: RecyclerView.ViewHolder>RecyclerView.setHorizontalAdapater(adapter: RecyclerView.Adapter<T>)
{
    this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    this.adapter = adapter
}
