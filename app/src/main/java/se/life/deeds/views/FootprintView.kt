package se.life.deeds.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import se.life.deeds.R
import se.life.deeds.application.DeedsApplication.Companion.getString
import java.text.DecimalFormat

/**
 * Created by christer on 2018-01-23.
 */
class FootprintView : View
{
    class Type(val value: Float, val bitmap: Bitmap)

    private lateinit var gradient: Paint
    private lateinit var transparent: Paint
    private lateinit var filled: Paint
    private lateinit var stroke: Paint
    private lateinit var smallText: Paint
    private lateinit var largeText: Paint
    private var w = 0
    private var h = 0
    private var info: List<Type>? = null
    private var footprint: Footprint? = null
    private val formatter = DecimalFormat("#.#")
    private val text0 = listOf(getString(R.string.view_footprint_your) ?: "", getString(R.string.view_footprint_footprint) ?: "")
    private val text1 = listOf(getString(R.string.view_footprint_ton) ?: "", getString(R.string.view_footprint_co2) ?: "")

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    {
        this.updatePaint()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    {
        this.updatePaint()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    {
        this.updatePaint()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int)
    {
        this.w = width
        this.h = height
        this.updateFootprint()
        this.updatePaint()
        super.onSizeChanged(w, h, oldw, oldh)
    }

    fun showInfo(info: List<Type>)
    {
        this.info = info
        this.updateFootprint()
        this.updatePaint()
    }

    var drawBonusInfo: Boolean = true

    private fun updateFootprint()
    {
        this.info?.let {
            this.footprint = Footprint(it.map { it.value }, this.w.toFloat())
        }
    }

    private fun updatePaint()
    {
        val colors = intArrayOf(Color.RED, Color.GREEN)
        val range = floatArrayOf(0.0f, 1.0f)
        val gradient = LinearGradient(0.0f, 0.0f, 0.0f, this.h.toFloat(), colors,  range, Shader.TileMode.CLAMP)
        val pattern = BitmapShader(BitmapFactory.decodeResource(resources, R.drawable.pattern), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)

        this.transparent = Paint()
        this.transparent.shader = pattern

        this.gradient = Paint()
        this.gradient.shader = gradient

        this.filled = Paint()
        this.filled.color = Color.WHITE
        this.filled.setShadowLayer(5f, 2f, 2f, Color.BLACK)
        setLayerType(LAYER_TYPE_SOFTWARE, this.filled)

        this.stroke = Paint()
        this.stroke.style = Paint.Style.STROKE
        this.stroke.strokeWidth = 5.0f
        this.stroke.color = Color.WHITE

        this.smallText = Paint()
        this.smallText.color = Color.BLACK

        this.largeText = Paint()
        this.largeText.color = Color.BLACK
        this.largeText.typeface = Typeface.DEFAULT_BOLD

        this.footprint?.let {
            this.smallText.textSize = (it.inner.radius * 2f) * 0.1f
            this.largeText.textSize = (it.inner.radius * 2f) * 0.2f
        }
    }

    override fun onDraw(canvas: Canvas?)
    {
        this.footprint?.let {footprint ->

            /*
             * Draw shape
             */
            val path = footprint.path(100f)
            canvas?.drawPath(path, this.stroke)
            canvas?.drawPath(path, this.transparent)

            if(this.drawBonusInfo)
            {
                /*
                 * Draw circles around with icons.
                 */
                this.info?.forEachIndexed { index, info ->

                    footprint.outer.getOrNull(index)?.let { circle ->
                        canvas?.drawCircle(circle.pos.x, circle.pos.y, circle.radius, this.filled)

                        val dst = RectF(circle.pos.x - circle.radius / 2f, circle.pos.y - circle.radius / 2f, circle.pos.x + circle.radius / 2f, circle.pos.y + circle.radius / 2f)
                        canvas?.drawBitmap(info.bitmap, null, dst, null)
                    }
                }

                /*
                 * Draw inner circle with text.
                 */
                canvas?.drawCircle(footprint.inner.pos.x, footprint.inner.pos.y, footprint.inner.radius, this.filled)

                val centerX = footprint.inner.pos.x
                val centerY = footprint.inner.pos.y - 6f
                val top = centerY - footprint.inner.radius
                val bottom = centerY + footprint.inner.radius
                val topTextY = top + this.smallText.textSize
                val bottomTextY = bottom - this.smallText.heightOfLines(this.text1) - this.smallText.textSize
                val total = this.formatter.format(this.info?.sumByDouble { it.value.toDouble() } ?: 0.0)

                canvas?.drawTextLinesCentered(this.text0, centerX, topTextY, this.smallText)
                canvas?.drawTextCenter(total, centerX, centerY - this.largeText.textSize / 2f, this.largeText)
                canvas?.drawTextLinesCentered(this.text1, centerX, bottomTextY, this.smallText)
            }
        }
    }
}

fun Canvas.drawTextCenter(text: String, x: Float, y: Float, paint: Paint)
{
    this.drawText(text, x - paint.measureText(text) / 2f, y + paint.textSize, paint)
}

fun Canvas.drawTextLinesCentered(text: List<String>, x: Float, y: Float, paint: Paint)
{
    var pos = y
    text.forEach {
        this.drawTextCenter(it, x, pos, paint)
        pos += paint.textSize
    }
}

fun Paint.heightOfLines(text: List<String>): Float
{
    return text.count().toFloat() * this.textSize
}
