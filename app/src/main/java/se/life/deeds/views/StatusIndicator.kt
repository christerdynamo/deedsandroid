package se.life.deeds.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.view_status_indicator.view.*
import se.life.deeds.R
import se.life.deeds.application.DeedsApplication.Companion.getString
import se.life.deeds.extensions.views.onClick

/**
 * Created by christer on 2018-01-23.
 *
 * Automatically hides all its children when showing a message.
 */
class StatusIndicator : RelativeLayout
{
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    {
        this.inflate(context)
        this.setAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    {
        this.inflate(context)
        this.setAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    {
        this.inflate(context)
        this.setAttrs(attrs)
    }

    private fun setAttrs(attrs: AttributeSet)
    {
        attrs.let {
            val array = context.obtainStyledAttributes(it, R.styleable.StatusIndicator, 0, 0)
            try
            {
                val text = array.getString(R.styleable.StatusIndicator_action)
                if(text.isNotEmpty())
                {
                    this.statusButton.text = text
                }
            }
            catch (e: Exception)
            {
            }
            array.recycle()
        }
    }

    private fun inflate(context: Context)
    {
        LayoutInflater.from(context).inflate(R.layout.view_status_indicator, this, true)
        this.setBackgroundColor(Color.TRANSPARENT)
        this.statusButton.onClick { this.action?.invoke() }
    }

    override fun onViewAdded(child: View?)
    {
        super.onViewAdded(child)

        /*
         * Makes sure the container is the top most child.
         */
        val indicator = this.statusContainer
        if(indicator != null && child != indicator)
        {
            val v = this.statusContainer
            this.removeViewInLayout(v)
            this.addView(v)
        }

        this.updateVisibility(child)
    }

    fun setMessage(resId: Int?)
    {
        this.statusMessage.text = getString(resId)
        this.updateChildren()
    }

    fun setMessage(text: String)
    {
        this.statusMessage.text = text
        this.updateChildren()
    }

    fun onUpdate(action: () -> Unit)
    {
        this.action = action
    }

    private var action: (() -> Unit)? = null

    private fun updateChildren()
    {
        var i = 0
        while(i < this.childCount)
        {
            this.updateVisibility(this.getChildAt(i))
            i++
        }
    }

    private fun updateVisibility(view: View?)
    {
        if(view == this.statusContainer)
        {
            if(this.statusMessage?.text?.length ?: 0 > 0)
            {
                view?.visibility = View.VISIBLE
            }
            else
            {
                view?.visibility = View.INVISIBLE
            }
        }
        else
        {
            if(this.statusMessage?.text?.length ?: 0 > 0)
            {
                view?.visibility = View.INVISIBLE
            }
            else
            {
                view?.visibility = View.VISIBLE
            }
        }
    }
}