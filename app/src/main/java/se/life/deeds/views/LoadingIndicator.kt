package se.life.deeds.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import se.life.deeds.R
import kotlinx.android.synthetic.main.view_loading_indicator.view.*


/**
 * Created by christer on 2018-01-23.
 *
 * Hides / shows all its children automatically when loading.
 */
class LoadingIndicator : RelativeLayout
{
    private var hidesChildren = true

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    {
        this.setAttrs(attrs)
        this.inflate(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    {
        this.setAttrs(attrs)
        this.inflate(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    {
        this.setAttrs(attrs)
        this.inflate(context)
    }

    private fun setAttrs(attrs: AttributeSet)
    {
        attrs.let {
            val array = context.obtainStyledAttributes(it, R.styleable.LoadingIndicator, 0, 0)
            try
            {
                this.hidesChildren = array.getBoolean(R.styleable.LoadingIndicator_hidesChildren, true)
            }
            catch (e: Exception)
            {
            }
            array.recycle()
        }
    }

    private fun inflate(context: Context)
    {
        LayoutInflater.from(context).inflate(R.layout.view_loading_indicator, this, true)
        this.setBackgroundColor(Color.TRANSPARENT)
    }

    override fun onViewAdded(child: View?)
    {
        super.onViewAdded(child)

        /*
         * Makes sure the container is the top most child.
         */
        val indicator = this.internalLoadingIndicator
        if(indicator != null && child != indicator)
        {
            val v = this.internalLoadingIndicator
            this.removeViewInLayout(v)
            this.addView(v)
        }

        updateVisibility(child)
    }

    var loading: Boolean = false
        set(value) {
            field = value
            this.updateChildren()
        }

    private fun updateChildren()
    {
        var i = 0
        while(i < this.childCount)
        {
            this.updateVisibility(this.getChildAt(i))
            i++
        }
    }

    private fun updateVisibility(view: View?)
    {
        if(view == this.internalLoadingIndicator)
        {
            if(this.loading)
            {
                view?.visibility = View.VISIBLE
            }
            else
            {
                view?.visibility = View.INVISIBLE
            }
        }
        else
        {
            if(this.loading && this.hidesChildren)
            {
                view?.visibility = View.INVISIBLE
            }
            else
            {
                view?.visibility = View.VISIBLE
            }
        }
    }
}