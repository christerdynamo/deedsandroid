package se.life.deeds.views

import android.graphics.Path
import android.graphics.PointF
import se.life.deeds.extensions.utils.*

/**
 * Created by christer on 2018-01-28.
 */
enum class Direction
{
    CLOCKWISE,
    ANTI_CLOCKWISE
}

class Footprint(val values: List<Float>, size: Float)
{
    val outer: List<Circle>
    val inner: Circle
    val figure: Figure

    init
    {
        val origin = PointF(size / 2.0f, size / 2.0f)
        val distance: (size: Float) -> Float = { it * 1.2f }
        val smoothness = size * 0.04f
        val min = size * 0.12f
        val max = size * 0.16f

        /*
         * Each circle represents one of the values.
         * The larger the circle the larger the value.
         */
        val circles = values.toPercentages().mapIndexed { index, value ->
            val scale = (max - min) * value + min
            val angle = 360.0f / values.count().toFloat() * index.toFloat()
            return@mapIndexed Circle(origin.move(angle, distance(scale)), scale)
        }

        /*
         * The circles with an icon within it.
         * Representing one of the values.
         */
        this.outer = circles.map {
            val scale = min / 2f
            val pos = it.pos.move(it.pos.angle(origin).degrees(), it.radius)
            return@map Circle(pos, scale)
        }

        /*
         * The while circle in the middle.
         */
        this.inner = Circle(origin, max)

        val trails = circles.mapIndexed { index, curr ->

            var indexPrev = index - 1
            var indexNext = index + 1

            if (indexPrev < 0) indexPrev = circles.count() - 1
            if (indexNext >= circles.count()) indexNext = 0

            val prev = circles[indexPrev]
            val next = circles[indexNext]

            /*
             * Finding the intersection points of the two large circles next to the current circle.
             */
            val i0 = curr.intersection(prev).furthestFrom(origin)
            val i1 = curr.intersection(next).furthestFrom(origin)

            /*
             * Position of the previous smoothing circle ending position.
             */
            val dp = i0.first.move(Direction.ANTI_CLOCKWISE, smoothness)

            /*
             * Calculating & creating the smoothing circle.
             */
            val d0 = i1.first.move(Direction.CLOCKWISE, smoothness)
            val d1 = i1.second.move(Direction.ANTI_CLOCKWISE, smoothness)
            val l0 = Line(curr.pos, d0.pos)
            val l1 = Line(next.pos, d1.pos)
            val tiny = Circle(l0.intersection(l1), l0.intersection(l1).distance(d0.pos))

            /*
             * Creating the trails.
             */
            val t0 = dp.circle.trail(dp.angle().degrees(), d0.angle().degrees(), Direction.ANTI_CLOCKWISE)
            val t1 = tiny.trail(d0.pos.angle(tiny.pos).degrees(), d1.pos.angle(tiny.pos).degrees(), Direction.CLOCKWISE)


            return@mapIndexed listOf(t0, t1)

        }.flatten()

        this.figure = Figure(trails)
    }

    public fun path(accuracy: Float): Path
    {
        val path = Path()
        path.moveTo(this.figure.point(0f).x, this.figure.point(0f).y)
        val points = accuracy
        var x = 0f
        while(x < 1.0f)
        {
            val pos = this.figure.point(x)
            path.lineTo(pos.x, pos.y)

            x += (1f / points)
        }
        path.close()
        return path
    }

    private fun List<Float>.toPercentages(): List<Float>
    {
        val lowest = this.min() ?: 0.0f
        val highest = (this.max() ?: 0.0f) - lowest

        return this.map {
            if (highest > 0.0f)
            {
                return@map (it - lowest) / highest
            }
            else
            {
                return@map 1.0f
            }
        }
    }

    class Figure(val trails: List<Trail>)
    {
        private val length: Float

        init
        {
            this.length = this.trails.sumByDouble { it.length.toDouble() }.toFloat()
        }

        /*
         * at = a value between 0.0 - 1.0
         */
        fun point(at: Float): PointF
        {
            val real = Math.max(Math.min(at, 1.0f), 0.0f) * this.length

            var x = 0.0f
            trails.forEach {
                if(real <= x + it.length)
                {
                    return it.path(real - x)
                }
                else
                {
                    x += it.length
                }
            }
            return PointF(0.0f, 0.0f)
        }
    }

    class Trail(val path: (Float) -> (PointF), val length: Float)

    private fun Pair<Circle.IntersectDot, Circle.IntersectDot>.furthestFrom(pos: PointF): Circle.IntersectDot
    {
        val d1 = this.first.pos.distance(pos)
        val d2 = this.second.pos.distance(pos)

        if(d1 > d2)
        {
            return this.first
        }
        else
        {
            return this.second
        }
    }

    class Line(val pos0: PointF, val pos1: PointF)
    {
        fun intersection(line: Line): PointF
        {
            val a1 = this.pos1.y - this.pos0.y
            val b1 = this.pos0.x - this.pos1.x
            val c1 = a1 * this.pos0.x + b1 * this.pos0.y

            val a2 = line.pos1.y - line.pos0.y
            val b2 = line.pos0.x - line.pos1.x
            val c2 = a2 * line.pos0.x + b2 * line.pos0.y

            val determinant = a1 * b2 - a2 * b1
            if(determinant == 0f)
            {
                return PointF(0f, 0f)
            }
            else
            {
                val x = (b2 * c1 - b1 * c2) / determinant
                val y = (a1 * c2 - a2 * c1) / determinant

                return PointF(x, y)
            }
        }
    }

    class Circle(val pos: PointF, val radius: Float)
    {
        /*
         * start = start angle in degrees
         * end = end angle in degrees
         */
        fun trail(start: Float, end: Float, direction: Direction): Trail
        {
            var s = start
            var e = end

            while(s < 0f) s += 360f
            while(s > 360f) s -= 360f

            while(e < 0f) e += 360f
            while(e > 360f) e -= 360f

            var length = 0f
            if(direction == Direction.CLOCKWISE)
            {
                length = s - e
            }
            else
            {
                length = e - s
            }

            while(length < 0f) length += 360f
            while(length > 360f) length -= 360f

            return Trail({

                val a: Float
                if(direction == Direction.CLOCKWISE)
                {
                    a = s - it
                }
                else
                {
                    a = s + it
                }

                val x = this.pos.x + this.radius * Math.cos(a.radians().toDouble()).toFloat()
                val y = this.pos.y + this.radius * Math.sin(a.radians().toDouble()).toFloat()

                return@Trail PointF(x, y)
            }, length)
        }

        class Dot(val circle: Circle, val pos: PointF)
        {
            fun move(direction: Direction, distance: Float): Dot
            {
                var angle = 0.0f
                if (direction == Direction.CLOCKWISE)
                {
                    angle = this.angle() - (distance / this.circle.radius)
                }
                else
                {
                    angle = this.angle() + (distance / this.circle.radius)
                }
                val x = this.circle.radius * Math.cos(angle.toDouble()).toFloat()
                val y = this.circle.radius * Math.sin(angle.toDouble()).toFloat()
                return Circle.Dot(this.circle, PointF(this.circle.pos.x + x, this.circle.pos.y + y))
            }

            fun angle(): Float
            {
                return Math.atan2(this.pos.y.toDouble() - this.circle.pos.y.toDouble(), this.pos.x.toDouble() - this.circle.pos.x.toDouble()).toFloat()
            }

            fun directionToward(pos: PointF): Direction
            {
                val d1 = this.move(Direction.CLOCKWISE, 0.001f).pos.distance(pos)
                val d2 = this.move(Direction.ANTI_CLOCKWISE, 0.001f).pos.distance(pos)

                if (d1 < d2)
                {
                    return Direction.CLOCKWISE
                }
                else
                {
                    return Direction.ANTI_CLOCKWISE
                }
            }
        }

        class IntersectDot(val first: Dot, val second: Dot)
        {
            val pos: PointF = this.first.pos
        }

        fun intersection(circle: Circle): Pair<IntersectDot, IntersectDot>
        {
            val intersections = this.intersect(this.pos, circle.pos, this.radius, circle.radius)

            val intersect0 = IntersectDot(Dot(this, intersections.first), Dot(circle, intersections.first))
            val intersect1 = IntersectDot(Dot(this, intersections.second), Dot(circle, intersections.second))

            return Pair(intersect0, intersect1)
        }

        private fun d(p1: PointF, p2: PointF): Float
        {
            return p1.distance(p2)
        }

        private fun l(p1: PointF, p2: PointF, r1: Float, r2: Float): Float
        {
            return ((Math.pow(r1.toDouble(), 2.0) - Math.pow(r2.toDouble(), 2.0) + Math.pow(d(p1, p2).toDouble(), 2.0)) / (2.0 * d(p1, p2).toDouble())).toFloat()
        }

        private fun h(p1: PointF, p2: PointF, r1: Float, r2: Float): Float
        {
            return Math.sqrt(Math.pow(r1.toDouble(), 2.0) - Math.pow(l(p1, p2, r1, r2).toDouble(), 2.0)).toFloat()
        }

        private fun intersect(p1: PointF, p2: PointF, r1: Float, r2: Float): Pair<PointF, PointF>
        {
            val x1 = (l(p1, p2, r1, r2) / d(p1, p2)) * (p2.x - p1.x) + (h(p1, p2, r1, r2) / d(p1, p2)) * (p2.y - p1.y) + p1.x
            val y1 = (l(p1, p2, r1, r2) / d(p1, p2)) * (p2.y - p1.y) - (h(p1, p2, r1, r2) / d(p1, p2)) * (p2.x - p1.x) + p1.y

            val x2 = (l(p1, p2, r1, r2) / d(p1, p2)) * (p2.x - p1.x) - (h(p1, p2, r1, r2) / d(p1, p2)) * (p2.y - p1.y) + p1.x
            val y2 = (l(p1, p2, r1, r2) / d(p1, p2)) * (p2.y - p1.y) + (h(p1, p2, r1, r2) / d(p1, p2)) * (p2.x - p1.x) + p1.y

            return Pair(PointF(x1, y1), PointF(x2, y2))
        }
    }
}