package se.life.deeds.utilities.promises

/**
 * Created by christer on 2018-01-23.
 */
abstract class Promise<V, E>
{
    abstract infix fun success(onSuccess: (V) -> Unit): Promise<V, E>
    abstract infix fun fail(onFailure: (E) -> Unit): Promise<V, E>
}
