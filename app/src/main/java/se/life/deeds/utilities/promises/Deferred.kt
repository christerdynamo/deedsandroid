package se.life.deeds.utilities.promises

import android.os.Handler
import se.life.deeds.application.DeedsApplication

/**
 * Created by christer on 2018-01-23.
 */
class Deferred<V, E>
{
    private var promise: InternalPromise<V, E>? = null

    fun promise(): Promise<V, E>
    {
        var promise = this.promise
        if(promise == null)
        {
            promise = InternalPromise<V, E>()
            this.promise = promise
            return promise
        }
        return promise
    }

    fun resolve(value: V)
    {
        if(this.promise?.value == null && this.promise?.error == null)
        {
            this.promise?.value = value
        }
    }

    fun reject(error: E)
    {
        if(this.promise?.value == null && this.promise?.error == null)
        {
            this.promise?.error = error
        }
    }

    private class InternalPromise<V, E>: Promise<V, E>()
    {
        var value: V? = null
            set(value)
            {
                field = value
                this.callListeners()
            }

        var error: E? = null
            set(value)
            {
                field = value
                this.callListeners()
            }

        var onSuccess = ArrayList<(V) -> Unit>()
        var onFailure = ArrayList<(E) -> Unit>()

        override fun success(onSuccess: (V) -> Unit): Promise<V, E>
        {
            val value = this.value
            if(value != null)
            {
                this.inMain {
                    onSuccess(value)
                }
            }
            else
            {
                this.onSuccess.add(onSuccess)
            }
            return this
        }

        override fun fail(onFailure: (E) -> Unit): Promise<V, E>
        {
            val error = this.error
            if(error != null)
            {
                this.inMain {
                    onFailure(error)
                }
            }
            else
            {
                this.onFailure.add(onFailure)
            }
            return this
        }

        private fun callListeners()
        {
            val value = this.value
            val error = this.error

            if(value != null)
            {
                for(listener in this.onSuccess)
                {
                    this.inMain {
                        listener(value)
                    }
                }
                this.onSuccess.clear()
            }
            else if(error != null)
            {
                for(listener in this.onFailure)
                {
                    this.inMain {
                        listener(error)
                    }
                }
                this.onFailure.clear()
            }
        }

        private fun inMain(on: () -> Unit)
        {
            val handler = Handler(DeedsApplication.context().mainLooper)
            val runnable = Runnable {
                on()
            }
            handler.post(runnable)
        }
    }
}
