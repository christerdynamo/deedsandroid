package se.life.deeds.helpers.dialog

import android.app.Activity
import android.app.AlertDialog
import se.life.deeds.application.DeedsApplication.Companion.getString

/**
 * Created by christer on 2018-01-23.
 */
class Dialog(activity: Activity, constructor: (DialogBuilderLight) -> (Unit))
{
    init
    {
        val builder = DialogBuilder(activity)
        constructor(builder)
        builder.show()
    }

    class DialogBuilder(activity: Activity): DialogBuilderLight(activity)
    {
        fun show()
        {
            if(!this.shown)
            {
                this.shown = true
                val builder = AlertDialog.Builder(this.activity)
                if(this.title != null)
                {
                    builder.setTitle(this.title)
                }
                builder.setItems(this.buttons.map { it.title }.toTypedArray(), { _, index ->
                    if(index < this.buttons.count())
                    {
                        this.buttons[index].action?.invoke()
                    }
                })
                builder.create().show()
            }
        }
    }

    open class DialogBuilderLight(activity: Activity)
    {
        protected val activity: Activity
        protected var title: String? = null
        protected var message: String? = null
        protected var buttons = ArrayList<AlertButton>()
        protected var shown = false

        init
        {
            this.activity = activity
        }

        fun title(title: String?): DialogBuilderLight
        {
            this.title = title
            return this
        }

        fun title(resId: Int?): DialogBuilderLight
        {
            this.title = getString(resId)
            return this
        }

        fun message(message: String?): DialogBuilderLight
        {
            this.message = message
            return this
        }

        fun message(resId: Int?): DialogBuilderLight
        {
            this.message = getString(resId)
            return this
        }

        fun button(resId: Int?, action: (() -> (Unit))? = null): DialogBuilderLight
        {
            return this.button(getString(resId), action)
        }

        fun button(title: String?, action: (() -> (Unit))? = null): DialogBuilderLight
        {
            if(title != null)
            {
                this.buttons.add(AlertButton(title, action))
            }
            return this
        }

        protected class AlertButton(title: String, action: (() -> (Unit))?)
        {
            var title: String
            var action: (() -> (Unit))? = null

            init
            {
                this.title = title
                this.action = action
            }
        }
    }
}
