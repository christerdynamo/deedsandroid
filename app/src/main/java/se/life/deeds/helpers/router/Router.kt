package se.life.deeds.helpers.router

import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import com.ncapdevi.fragnav.FragNavController
import com.ncapdevi.fragnav.FragNavTransactionOptions

/**
 * Created by christer on 2018-01-23
 */
object Router
{
    private var fragNavController: FragNavController? = null
    
    fun set(controller: FragNavController)
    {
        this.fragNavController = controller
    }
    
    fun push(fragment: Fragment?)
    {
        if (fragment != null)
        {
            this.fragNavController?.pushFragment(fragment)
        }
    }
    
    fun pop(): Boolean
    {
        try
        {
            this.fragNavController?.popFragment()
            return true
        }
        catch (e: Exception)
        {
        }
        return false
    }
    
    fun popToRoot()
    {
        this.fragNavController?.clearStack()
    }
    
    fun show(fragment: Fragment?)
    {
        if (fragment != null && fragment is DialogFragment)
        {
            this.fragNavController?.showDialogFragment(fragment)
        }
    }
    
    fun dismiss()
    {
        this.fragNavController?.clearDialogFragment()
    }
    
    fun switchTab(i: Int)
    {
        try
        {
            this.fragNavController?.switchTab(i, FragNavTransactionOptions.newBuilder().build())
        }
        catch (e: Exception)
        {
        }
    }
}
