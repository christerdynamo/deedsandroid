package se.life.deeds.application

import android.app.Application
import android.content.Context

/**
 * Created by christer on 2018-01-23.
 */
class DeedsApplication : Application()
{
    override fun onCreate()
    {
        super.onCreate()
        instance = this
    }

    companion object
    {
        lateinit var instance: DeedsApplication
            private set

        fun context(): Context
        {
            return instance.applicationContext
        }

        fun getString(resId: Int?): String?
        {
            if(resId != null)
            {
                return context().getString(resId)
            }
            return null
        }
    }
}
