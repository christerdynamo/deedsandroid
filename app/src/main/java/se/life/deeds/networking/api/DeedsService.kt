package se.life.deeds.networking.api

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import se.life.deeds.BuildConfig
import se.life.deeds.networking.api.interceptors.AddCookiesInterceptor
import se.life.deeds.networking.api.interceptors.LogInterceptor
import se.life.deeds.networking.api.interceptors.ReceivedCookiesInterceptor

/**
 * Created by christer on 2018-01-23.
 */
class DeedsService
{
    companion object
    {
        val service = this.make()

        private fun make(): DeedsServiceInterface
        {
            val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create()

            val client = OkHttpClient.Builder()
                    .addInterceptor(AddCookiesInterceptor())
                    .addInterceptor(ReceivedCookiesInterceptor())
                    .addInterceptor(LogInterceptor())
                    .build()

            return Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
                    .create(DeedsServiceInterface::class.java)
        }
    }
}