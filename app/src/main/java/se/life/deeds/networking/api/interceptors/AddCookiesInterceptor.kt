package se.life.deeds.networking.api.interceptors

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import se.life.deeds.application.DeedsApplication.Companion.context

/**
 * Created by christer on 2017-11-17.
 */
class AddCookiesInterceptor: Interceptor
{
    override fun intercept(chain: Interceptor.Chain?): Response
    {
        val builder = chain!!.request().newBuilder()
        val preferences = context().getSharedPreferences("cookies", Context.MODE_PRIVATE).getStringSet("cookies", HashSet<String>())
        for (cookie in preferences)
        {
            builder.addHeader("Cookie", cookie)
        }
        return chain.proceed(builder.build())
    }
}
