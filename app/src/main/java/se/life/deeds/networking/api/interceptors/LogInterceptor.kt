package se.life.deeds.networking.api.interceptors

import android.util.Log
import se.life.deeds.extensions.networking.curl
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okio.Buffer
import java.io.EOFException
import java.nio.charset.Charset

/**
 * Created by christer on 2017-11-08.
 */
class LogInterceptor: Interceptor
{
    override fun intercept(chain: Interceptor.Chain?): Response
    {
        val request = chain!!.request()
        val response = chain.proceed(request)
        if(!response!!.isSuccessful)
        {
            Log.v("Network", ">>>>>>>>>>>")
            Log.d("Network", String.format("%s (%s)\n%s", request.url(), response.code(), response.curl()))
            Log.v("Network", String.format("sent headers:\n    %s", this.headers(request.headers())))
            Log.v("Network", String.format("sent body:\n    %s", this.body(request)))
            Log.v("Network", String.format("received headers:\n    %s", this.headers(response.headers())))
            Log.v("Network", String.format("received body:\n    %s", this.response(response)))
            Log.v("Network", "<<<<<<<<<<<")
        }
        else
        {
            Log.v("Network", ">>>>>>>>>>>")
            Log.v("Network", String.format("%s (%s)\n%s", request.url(), response.code(), response.curl()))
            Log.v("Network", String.format("sent headers:\n    %s", this.headers(request.headers())))
            Log.v("Network", String.format("sent body:\n    %s", this.body(request)))
            Log.v("Network", String.format("received headers:\n    %s", this.headers(response.headers())))
            Log.v("Network", String.format("received body:\n    %s", this.response(response)))
            Log.v("Network", "<<<<<<<<<<<")
        }
        return response
    }
    
    private fun headers(headers: Headers): String
    {
        var text = ""
        if(headers.size() > 0)
        {
            for (i in 0..(headers.size() - 1))
            {
                text += headers.name(i) + ": " + headers.value(i)
                if(i < headers.size() - 1)
                {
                    text += "\n    "
                }
            }
        }
        
        if(text.isNotEmpty()) return text
        return "<none>"
    }
    
    private fun body(request: Request): String
    {
        if(request.body() != null)
        {
            try
            {
                val buffer = Buffer()
                request.body()!!.writeTo(buffer)
                if (this.isPlaintext(buffer))
                {
                    return buffer.readString(Charset.defaultCharset())
                }
                else if (buffer.size() > 0)
                {
                    return "<binary>"
                }
            }
            catch (e: EOFException)
            {
                return "<failed read>"
            }
        }
        return "<none>"
    }
    
    
    private fun response(response: Response): String
    {
        if(response.body() != null)
        {
            try
            {
                val source = response.body()!!.source()
                source.request(Long.MAX_VALUE)
                val buffer = source.buffer()
                if(this.isPlaintext(buffer))
                {
                    return buffer.clone().readString(Charset.defaultCharset())
                }
                else if(buffer.size() > 0)
                {
                    return "<binary>"
                }
            }
            catch (e: EOFException)
            {
                return "<failed read>"
            }
        }
        return "<none>"
    }
    
    private fun isPlaintext(buffer: Buffer): Boolean
    {
        try
        {
            val prefix = Buffer()
            val byteCount = (if (buffer.size() < 64) buffer.size() else 64).toLong()
            buffer.copyTo(prefix, 0, byteCount)
            for (i in 0..15)
            {
                if (prefix.exhausted())
                {
                    break
                }
                val codePoint = prefix.readUtf8CodePoint()
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint))
                {
                    return false
                }
            }
            return true
        }
        catch (e: EOFException)
        {
            return false // Truncated UTF-8 sequence.
        }
    }
}
