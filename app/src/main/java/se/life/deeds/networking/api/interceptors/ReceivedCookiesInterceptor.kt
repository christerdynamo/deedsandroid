package se.life.deeds.networking.api.interceptors

import android.content.Context
import se.life.deeds.application.DeedsApplication.Companion.context
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by christer on 2017-11-17.
 */
class ReceivedCookiesInterceptor: Interceptor
{
    override fun intercept(chain: Interceptor.Chain?): Response
    {
        val originalResponse = chain!!.proceed(chain.request())

        if (!originalResponse.headers("Set-Cookie").isEmpty())
        {
            val cookies = HashSet<String>()
            for (header in originalResponse.headers("Set-Cookie"))
            {
                cookies.add(header)
            }

            context().getSharedPreferences("cookies", Context.MODE_PRIVATE)
                    .edit()
                    .putStringSet("cookies", cookies)
                    .apply()
        }

        return originalResponse
    }
}
