
A module represents a single screen or sub-screen within the app. Each module should be self-contained meaning that no code in a module should depend on another module's existence.

A module is consists of:
* View (Fragment)
  * Updates interface, forwards user actions to the presenter.
* Presenter
  * Main logic of each module. Tells what the view should show, but does not know exactly how the information is shown on the screen. Receives data from the interactor, prepares for the view. The presenter is also responsible for notifying the wireframe to navigate to a new module.
* Interactor
  * Responsible of getting any data required by presenter. Although not responsible of adjusting any data to the UI. For example sorting.
* Wireframe
  * Handles all navigation related code.
